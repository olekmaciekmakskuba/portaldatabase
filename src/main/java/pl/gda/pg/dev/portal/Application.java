package pl.gda.pg.dev.portal;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.gda.pg.commons.vertx.VertxWrapper;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class Application {

    @Bean
    public VertxWrapper getEventBusWrapper() throws ExecutionException, InterruptedException {
        CompletableFuture<Vertx> vertxFuture = getVertxAsync();
        Vertx vertx = vertxFuture.get();

        return new VertxWrapper(vertx);
    }

    private CompletableFuture<Vertx> getVertxAsync() {
        CompletableFuture<Vertx> future = new CompletableFuture<>();

        VertxOptions options = new VertxOptions();
        options.setClusterHost("localhost");
        options.setHAEnabled(true);
        Vertx.clusteredVertx(options, event -> future.complete(event.result()));

        return future;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.print("done");
    }
}
