package pl.gda.pg.dev.portal.database.submission.file;

import org.springframework.stereotype.Component;
import pl.gda.pg.shared.data.File;

@Component
public class SubmissionFileBuilder {

    public SubmissionFile create(File file) {
        SubmissionFile submissionFile = new SubmissionFile();

        submissionFile.setName(file.getName());
        submissionFile.setContent(file.getContent());
        submissionFile.setFolder(file.getFolder());

        return submissionFile;
    }

}
