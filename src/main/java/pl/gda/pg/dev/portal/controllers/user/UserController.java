package pl.gda.pg.dev.portal.controllers.user;

import pl.gda.pg.dev.portal.database.role.Role;
import pl.gda.pg.dev.portal.database.role.RolesDao;
import pl.gda.pg.dev.portal.database.user.User;
import pl.gda.pg.dev.portal.database.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RolesDao rolesDao;

    public String create(String email, String name, String password, String role) {
        try {
            Role userRole = rolesDao.findByName(role);
            User user = new User(email, password, name, userRole);
            userDao.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Created";
    }

    public String delete(Long id) {
        try {
            User user = new User(id);
            userDao.delete(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Deleted";
    }
}
