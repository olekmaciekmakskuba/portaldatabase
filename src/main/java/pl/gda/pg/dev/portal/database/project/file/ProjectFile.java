package pl.gda.pg.dev.portal.database.project.file;

import pl.gda.pg.dev.portal.database.project.Project;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "project_file")
public class ProjectFile {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    @NotNull
    private String folder;

    @ManyToOne
    @JoinColumn(name = "projectID")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getFolder() {
        return folder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
