package pl.gda.pg.dev.portal.database.submission;

import pl.gda.pg.dev.portal.database.submission.detail.SubmissionDetailData;
import pl.gda.pg.dev.portal.database.submission.file.SubmissionFile;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "submission")
public class Submission {

    @Id
    @GeneratedValue
    private int submissionID;

    private int rating;

    @OneToMany(mappedBy = "submission")
    @Fetch(FetchMode.JOIN)
    private Set<SubmissionFile> fileSet;

    @OneToMany(mappedBy = "submission")
    @Fetch(FetchMode.JOIN)
    private Set<SubmissionDetailData> details;

    public long getSubmissionID() {
        return submissionID;
    }

    public void setSubmissionID(int submissionID) {
        this.submissionID = submissionID;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Set<SubmissionFile> getFileSet() {
        return fileSet;
    }

    public void setFileSet(Set<SubmissionFile> fileSet) {
        this.fileSet = fileSet;
    }

    public Set<SubmissionDetailData> getDetails() {
        return details;
    }

    public void setDetails(Set<SubmissionDetailData> details) {
        this.details = details;
    }
}

