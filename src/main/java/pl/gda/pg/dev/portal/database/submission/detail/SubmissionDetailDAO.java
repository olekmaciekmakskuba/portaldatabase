package pl.gda.pg.dev.portal.database.submission.detail;

import org.springframework.data.repository.CrudRepository;

public interface SubmissionDetailDAO extends CrudRepository<SubmissionDetailData, Long>{
}
