package pl.gda.pg.dev.portal.database.submission.detail;

import pl.gda.pg.dev.portal.database.submission.Submission;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "submission_detail")
public class SubmissionDetailData {

    @Id
    @GeneratedValue
    private long submissionDetailID;

    @NotNull
    private String testName;

    @ManyToOne
    @JoinColumn (name = "submissionID")
    private Submission submission;

    private boolean passed;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public long getSubmissionDetailID() {
        return submissionDetailID;
    }

    public void setSubmissionDetailID(long submissionDetailID) {
        this.submissionDetailID = submissionDetailID;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }
}
