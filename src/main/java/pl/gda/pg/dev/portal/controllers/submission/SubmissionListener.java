package pl.gda.pg.dev.portal.controllers.submission;

import pl.gda.pg.dev.portal.address.VertxServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.gda.pg.commons.json.serializer.JsonObjectSerializer;
import pl.gda.pg.commons.vertx.VertxWrapper;
import pl.gda.pg.shared.data.SubmissionRequest;
import pl.gda.pg.shared.data.SubmissionResponse;

import javax.annotation.PostConstruct;

@Component
public class SubmissionListener {

    @Autowired
    private VertxWrapper vertxWrapper;
    @Autowired
    private SubmissionController submissionController;

    @PostConstruct
    private void init() {
        vertxWrapper.eventBusWrapper().consumer(VertxServices.DATABASE_SUBMISSION_UPDATE.getAddress(), submissionController::updateSubmission, SubmissionResponse.class);
        vertxWrapper.eventBusWrapper().consumer(VertxServices.DATABASE_SUBMISSION_SAVE.getAddress(), submissionController::saveSubmission, SubmissionRequest.class);

        vertxWrapper.eventBus().consumer(VertxServices.DATABASE_SUBMISSION_FIND.getAddress(), message -> {
            JsonObjectSerializer jsonObjectSerializer = JsonObjectSerializer.getInstance();
            SubmissionRequest submission = submissionController.findSubmission((int) message.body());

            message.reply(jsonObjectSerializer.serializeObject(submission));
        });

    }
}
