package pl.gda.pg.dev.portal.database.submission.detail;

import org.springframework.stereotype.Component;
import pl.gda.pg.shared.data.SubmissionDetail;

@Component
public class SubmissionDetailBuilder {

    public SubmissionDetailData create(SubmissionDetail submissionDetail) {
        SubmissionDetailData detail = new SubmissionDetailData();

        detail.setTestName(submissionDetail.getTestName());
        detail.setMessage(submissionDetail.getMessage());
        detail.setPassed(submissionDetail.isPassed());

        return detail;
    }



}
