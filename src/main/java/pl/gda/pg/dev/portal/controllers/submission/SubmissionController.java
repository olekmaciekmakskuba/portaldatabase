package pl.gda.pg.dev.portal.controllers.submission;

import pl.gda.pg.dev.portal.database.submission.Submission;
import pl.gda.pg.dev.portal.database.submission.SubmissionDAO;
import pl.gda.pg.dev.portal.database.submission.detail.SubmissionDetailBuilder;
import pl.gda.pg.dev.portal.database.submission.detail.SubmissionDetailDAO;
import pl.gda.pg.dev.portal.database.submission.detail.SubmissionDetailData;
import pl.gda.pg.dev.portal.database.submission.file.SubmissionFile;
import pl.gda.pg.dev.portal.database.submission.file.SubmissionFileBuilder;
import pl.gda.pg.dev.portal.database.submission.file.SubmissionFileDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.gda.pg.shared.data.File;
import pl.gda.pg.shared.data.SubmissionRequest;
import pl.gda.pg.shared.data.SubmissionResponse;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class SubmissionController {

    @Autowired
    private SubmissionDAO submissionDAO;
    @Autowired
    private SubmissionDetailDAO submissionDetailDAO;
    @Autowired
    private SubmissionFileDAO submissionFileDAO;

    @Autowired
    private SubmissionDetailBuilder submissionDetailBuilder;
    @Autowired
    private SubmissionFileBuilder submissionFileBuilder;

    public SubmissionRequest findSubmission(int id) {
        Submission one = submissionDAO.findOne(id);
        SubmissionRequest submission = new SubmissionRequest();

        List<File> fileList = one.getFileSet()
                .stream()
                .map(this::createFile)
                .collect(Collectors.toList());

        submission.setFiles(fileList);
        submission.setId((int) one.getSubmissionID());

        return submission;
    }

    private File createFile(SubmissionFile submissionFile) {
        File file = new File();
        file.setContent(submissionFile.getContent());
        file.setName(submissionFile.getName());
        file.setFolder(submissionFile.getFolder());
        return file;
    }

    public void updateSubmission(SubmissionResponse submissionResponse) {
        Submission submission = submissionDAO.findOne(submissionResponse.getId());

        Set<SubmissionDetailData> details = submissionResponse.getDetails()
                .stream()
                .map(submissionDetailBuilder::create)
                .collect(Collectors.toSet());

        details.forEach(d -> d.setSubmission(submission));
        details.forEach(submissionDetailDAO::save);

        submission.setRating(5);
        submissionDAO.save(submission);
    }

    public void saveSubmission(SubmissionRequest request) {
        Submission submission = new Submission();
        submissionDAO.save(submission);

        Set<SubmissionFile> files = request.getFiles()
                .stream()
                .map(submissionFileBuilder::create)
                .collect(Collectors.toSet());

        files.forEach(s -> s.setSubmission(submission));
        files.forEach(submissionFileDAO::save);
    }
}