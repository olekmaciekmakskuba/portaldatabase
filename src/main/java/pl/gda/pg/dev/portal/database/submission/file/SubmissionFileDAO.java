package pl.gda.pg.dev.portal.database.submission.file;

import org.springframework.data.repository.CrudRepository;

public interface SubmissionFileDAO extends CrudRepository<SubmissionFile, Long>{
}
