package pl.gda.pg.dev.portal.database.project.file;

import org.springframework.data.repository.CrudRepository;

public interface ProjectFileDAO extends CrudRepository<ProjectFile, Long> {
}
