package pl.gda.pg.dev.portal.database.submission;

import org.springframework.data.repository.CrudRepository;

public interface SubmissionDAO extends CrudRepository<Submission, Integer>{
}
