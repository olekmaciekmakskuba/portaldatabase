package pl.gda.pg.dev.portal.database.project;

import org.springframework.data.repository.CrudRepository;

public interface ProjectDAO extends CrudRepository<Project, Long> {
}
