package pl.gda.pg.dev.portal.database.project;

import pl.gda.pg.dev.portal.database.project.file.ProjectFile;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue
    private long projectID;

    @NotNull
    private String name;

    @OneToMany(mappedBy = "project")
    @Fetch(FetchMode.SELECT)
    private Set<ProjectFile> files;

    public long getProjectID() {
        return projectID;
    }

    public void setProjectID(long projectID) {
        this.projectID = projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ProjectFile> getFiles() {
        return files;
    }

    public void setFiles(Set<ProjectFile> files) {
        this.files = files;
    }
}
