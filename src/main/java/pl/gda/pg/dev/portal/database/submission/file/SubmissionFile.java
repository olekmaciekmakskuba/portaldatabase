package pl.gda.pg.dev.portal.database.submission.file;

import pl.gda.pg.dev.portal.database.submission.Submission;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "file_submission")
public class SubmissionFile {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String content;

    @NotNull
    private String folder;

    @ManyToOne
    @JoinColumn(name = "submissionID")
    private Submission submission;

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    public String getFolder() {
        return folder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
