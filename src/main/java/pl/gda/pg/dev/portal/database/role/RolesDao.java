package pl.gda.pg.dev.portal.database.role;

import org.springframework.data.repository.CrudRepository;

public interface RolesDao extends CrudRepository<Role, Integer> {
    public Role findByName(String name);
}
